import java.util.LinkedList;

public class RowerList {

    public static void main(String[] args) {
        Rower rower = new Rower("Czerwony", "ALPINE", 10, true);
        Rower rower1 = new Rower("Czarny", "ALPINE", 10, false);
        Rower rower2 = new Rower("Czerwony", "BMX", 11, true);
        Rower rower3 = new Rower("Czarny", "BMX", 11, false);
        Rower rower4 = new Rower("Czerwony", "KTM", 12, true);
        Rower rower5 = new Rower("Czarny", "KTM", 12, false);
        Rower rower6 = new Rower("Czerwony", "JOYKE", 13, true);
        Rower rower7 = new Rower("Czerwony", "JOYKE", 13, false);
        Rower rower8 = new Rower("Złoty", "KETTLER", 15, true);

        LinkedList<Rower> rowerList = new LinkedList<Rower>();
        rowerList.add(rower);
        rowerList.add(rower1);
        rowerList.add(rower2);
        rowerList.add(rower3);
        rowerList.add(rower4);
        rowerList.add(rower5);
        rowerList.add(rower6);
        rowerList.add(rower7);
        rowerList.add(rower8);


        rower.wybierzMarke("ALPINE", "BMX", "KTM", "JOYKE", "KETTER");
    }
}
