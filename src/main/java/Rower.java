public class Rower extends Wypozyczalnia {
    private String kolor;
    private String marka;
    private int cena;
    private boolean status;
    private long startCzasu;
    private long koniecCzasu;


    public Rower(String kolor, String marka, int cena, boolean status)
    {
        this.kolor = kolor;
        this.marka = marka;
        this.cena = cena;
        this.status = status;
    }

    public String getKolor() {
        return kolor;
    }

    public void setKolor(String kolor) {
        this.kolor = kolor;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public long getStartCzasu() {
        return startCzasu;
    }

    public void setStartCzasu(long startCzasu) {
        this.startCzasu = startCzasu;
    }

    public long getKoniecCzasu() {
        return koniecCzasu;
    }

    public void setKoniecCzasu(long koniecCzasu) {
        this.koniecCzasu = koniecCzasu;
    }

    void wybierzMarke(String marka1, String marka2, String marka3, String marka4, String marka5)
    {
        marka1 = "ALPINE";
        marka2 = "ROMET";
        marka3 = "KETTER";
        marka4 = "SETBY";
    }

    @Override
    public String toString()
    {
        return
                "wybrana konfiguracja \n" +
                        "MARKA: " + marka +
                        "\n KOLOR: " + kolor +
                        "\n STATUS " + status +
                        "\n KWOTA " + cena;
    }
}
